__version__ = '0.9.7'

import time
import random

# import SPI output handling - hardware dependent
import ledSPIout

# import character arrays definded in fonts
from font_7x5 import char_7x5
from font_5x3 import char_5x3

# gamma correction data
from gamma import gammaLinear, gammaAdafruit, gammaPimoroni
gamma = gammaLinear[:]

# hardware design - LRLR / LRRL / RLLR - odd rows even rows, left to right or right to left
matrixHWdefault='LRRL'
# power consumption and limitation - values in mA
power_per_pixel = 20
max_power = 3000

# some other limits 
max_strip_pixels = 1000
max_led_rows = 15
max_led_columns = 99
max_matrix_rows = 79
max_matrix_columns = 300

# selection of gamma table values
def set_gamma_method( use=None ):
    global gamma
    if use == 'Pimoroni':
        gamma = gammaPimoroni[:]
    elif use == 'Adafruit':
        gamma = gammaAdafruit[:] 
    else:
        gamma = gammaLinear[:]
            
# supported Led strip type: SK9822
# physical output with SPI on Raspberry PI
def ledStripUpdate( pixelType, pixelData ):
    if pixelType in [ 'SK9822','APA102C' ]:
        # start frame
        databytes = [ 0x00, 0x00, 0x00, 0x00 ]
        # pixel data
        databytes += pixelData
        # refresh bytes for SK9822
        databytes += [ 0x00, 0x00, 0x00, 0x00 ]
        # additional clocks ticks - 1/2 per pixel
        for x in range ( ( ( ( len( pixelData ) // 4 ) - 1 ) // 2 + 1 ) // 8 + 1 ):
            databytes += [ 0x00 ]
        # write to LED strip using SPI
        ledSPIout.SPIwrite(databytes)
    if pixelType in [ 'NeoPixel' ]:
        pass
# smallest unit = pixel
class Pixel:
    """ a single pixel of a strip of pixels with RGB leds """

    def __init__(self, red=0, green=0, blue=0, brightness=127, pixelType=None):
        """ allowed pixel types = SK9822 | APA102C | NeoPixel """
        assert pixelType in [ 'SK9822','APA102C','NeoPixel' ], \
               "pixel type must be SK9822 | APA102C | NeoPixel"
        self.pixelType = pixelType

        """ new pixel: set initial values """
        self.set2color(red, green, blue, brightness)
        
    def set2color(self, red=0, green=0, blue=0, brightness=None):
        """ red led - allowed numeric input values are 0..255 """         
        assert type(red) is int and 0 <= red  <= 255 , "red value must be 0..255"
        self.red = red

        """ green led - allowed numeric input values are 0..255 """         
        if type(green) is not int or green < 0 or green > 255:
            raise TypeError("green value must be 0..255")
        self.green = green

        """ blue led - allowed numeric input values are 0..255 """         
        if type(blue) is not int or blue < 0 or blue > 255:
            raise TypeError("blue value must be 0..255")
        self.blue = blue

        """ optional: general brightness of the led - allowed numeric input values are 0..255 """         
        if brightness is not None:
            if type(brightness) is not int or brightness < 0 or brightness > 255:
                raise TypeError("brightness value must be 0..255")
            self.brightness = brightness

        """ gamma correction """
        g_red = gamma[ self.red * self.brightness // 255 ]
        g_green = gamma[ self.green * self.brightness // 255 ]
        g_blue = gamma[ self.blue * self.brightness // 255 ]

        """ calculation of the power consumption in mA """
        self.power_used = ( ( g_red + g_green + g_blue ) / 255 ) * power_per_pixel
        
        if self.pixelType in [ 'SK9822','APA102C' ]:
            """ calculation of the databytes for the pixel refresh - type SK9822 """
            if self.brightness == 0:
                self.databytes = [ 0xE0, 0x00, 0x00, 0x00 ]
            else:
                p_brightness = ( self.brightness // 8 ) | 0xE0
                p_red = ( g_red * 255 ) // self.brightness & 0xFF
                p_green = ( g_green * 255 ) // self.brightness & 0xFF
                p_blue = ( g_blue * 255 ) // self.brightness & 0xFF
                self.databytes = [ p_brightness, p_blue, p_green, p_red ]
        elif self.pixelType in [ 'NeoPixel' ]:
            """ calculation of the databytes for the pixel refresh - type WS2812 """
            p_red = g_red & 0xFF
            p_green = g_green & 0xFF
            p_blue = g_blue & 0xFF
            self.databytes = [ p_blue, p_green, p_red ]
                
    def change2color(self, new_red=0, new_green=0, new_blue=0, new_brightness=None ):
        """ change RGB - only if not all were off """
        if self.red > 0 or self.green > 0 or self.blue > 0:
            """ change brightness only if provided explicitely """
            if new_brightness == None: 
                brightness = self.brightness
            else:
                brightness = new_brightness
            self.set2color( new_red, new_green, new_blue, brightness )

    def set2brightness(self, brightness=127):
        """ set new brightness, keep current RGB """
        self.set2color(self.red, self.green, self.blue, brightness)

    def set2off(self):
        self.set2color(0, 0, 0)
        
    def set2red(self):
        self.set2color(red=255)
        
    def set2green(self):
        self.set2color(green=255)
        
    def set2blue(self):
        self.set2color(blue=255)
        
    def set2yellow(self):
        self.set2color(red=127, green=127)
        
    def set2cyan(self):
        self.set2color(blue=127, green=127)
        
    def set2magenta(self):
        self.set2color(blue=127, red=127)

    def set2white(self):
        self.set2color(85, 85, 85)
        
    def set2fullwhite(self):
        self.set2color(255, 255, 255)
        
    def set2random(self):
        red = random.randint(64, 255)
        green = random.randint(64, 255)
        blue = random.randint(64, 255)
        self.set2color(red, green, blue)


# strip = line of pixels
class LedStrip:
    """ define a strip as a line of pixels """
    def __init__( self, pixels=5, 
                  red=0, green=0, blue=0, brightness=127, 
                  pixelType='SK9822',power_limit=max_power  ):
        """ limit number of requested pixels """
        if not ( type(pixels) is int and 2 <= pixels <= max_strip_pixels ):
            raise TypeError("LedStrip: incorrect value of pixels" )
        self.pixels = pixels
        assert pixelType in [ 'SK9822','APA102C','NeoPixel' ], \
               "pixel type must be SK9822 | APA102C | NeoPixel"
        self.pixelType = pixelType
        """ limit power consumption of the strip """
        if not ( type(power_limit) is int and 500 <= power_limit <= max_power ):
            raise TypeError("LedStrip: incorrect value of power_limit" )
        self.power_limit = power_limit
        """ create strip object as array of Pixel objects """
        self.pixel = [ Pixel( red, green, blue, brightness, pixelType )
                       for x in range ( pixels ) ]

    # some settings impacting all pixel        

    def setall2off(self):
        [ self.pixel[x].set2off( ) for x in range ( self.pixels ) ]
        
    def setall2color(self, red=0, green=0, blue=0, brightness=None):
        [ self.pixel[x].set2color(red, green, blue, brightness ) for x in range ( self.pixels ) ]
        
    def changeall2color(self, new_red=0, new_green=0, new_blue=0, brightness=None):
        [ self.pixel[x].change2color(new_red, new_green, new_blue, brightness ) for x in range ( self.pixels ) ]
        
    def setall2brightness(self, brightness=127):
        [ self.pixel[x].set2brightness( brightness ) for x in range ( self.pixels ) ]
        
    def setall2random(self):
        [ self.pixel[x].set2random( ) for x in range ( self.pixels ) ]
        
    # some settings for a dedicated pixel        

    def set2off(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2off( )

    def set2color(self, pixel=None, red=0, green=0, blue=0, brightness=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2color(red, green, blue, brightness)

    def change2color(self, pixel=None, new_red=0, new_green=0, new_blue=0, new_brightness=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].change2color(new_red, new_green, new_blue, new_brightness)

    def set2brightness(self, pixel=None, brightness=127):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels - 1 ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel].set2brightness( brightness ) 
        
    def set2red(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2red( )

    def set2green(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2green( )

    def set2blue(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2blue( )

    def set2yellow(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2yellow( )

    def set2magenta(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2magenta( )

    def set2cyan(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2cyan( )

    def set2white(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2white( )

    def set2fullwhite(self, pixel=None):
        if not ( type(pixel) is int and 1 <= pixel <= self.pixels ):
            raise TypeError("invalid pixel value")
        self.pixel[pixel-1].set2fullwhite( )

    # moving pixels on the strip              

    def rotate_left(self):
        self.pixel.append(self.pixel.pop(0))
             
    def rotate_right(self):
        self.pixel.insert(0,self.pixel.pop(self.pixels - 1))
             
    def flip(self):
        self.pixel.reverse()
                 
    # physical output
        
    def show(self):
        # power consumption
        power_total = 0
        # data frames - blank leds if power limit reached
        pixelData = []
        for x in self.pixel:
            power_total += x.power_used
            if power_total <= self.power_limit:
                pixelData += x.databytes
            else:
                if self.pixelType in [ 'SK9822','APA102C']:
                    pixelData += [ 0xF0, 0x00, 0x00, 0x00 ]
                if self.pixelType in [ 'NeoPixel']:
                    pixelData += [ 0x00, 0x00, 0x00 ]
        # sent to output
        ledStripUpdate(self.pixelType, pixelData)

class LedVirtualMatrix:
    """ define a matrix and display it partially on an array of strips """
    def __init__( self,
                  rows=30, columns=300,
                  ledRows=7, ledColumns=30,
                  pixelType='SK9822', matrixHW=matrixHWdefault, power_limit=max_power):
        """ limit number of requested rows """
        if type( rows ) is not int or rows < 2 or rows > max_matrix_rows:
            raise TypeError("LedMatrix: rows value must be 2..",max_matrix_rows)
        self.rows = rows
        """ limit number of requested columns """
        if type( columns ) is not int or columns < 5 or columns > max_matrix_columns:
            raise TypeError("LedMatrix: columns value must be 5..",max_matrix_columns)
        self.columns = columns
        """ limit number of requested led rows """
        if type( ledRows ) is not int or ledRows < 2 or ledRows > max_led_rows:
            raise TypeError("LedMatrix: led rows value must be 2..",max_led_rows)
        self.ledRows = ledRows
        """ limit number of requested led columns """
        if type( ledColumns ) is not int or ledColumns < 5 or ledColumns > max_led_columns:
            raise TypeError("LedMatrix: led columns value must be 5..",max_led_columns)
        self.ledColumns = ledColumns
        assert pixelType in [ 'SK9822','APA102C','NeoPixel' ], \
               "pixel type must be SK9822 | APA102C | NeoPixel"
        self.pixelType = pixelType
        """ limit power consumption of the strip """
        if not ( type(power_limit) is int and 500 <= power_limit <= max_power ):
            raise TypeError("LedMatrix: incorrect value of power_limit" )
        self.power_limit = power_limit
        """ check virtual sizes >= led sizes """
        if self.ledRows > self.rows:
            raise TypeError("LedMatrix: rows value must be >= led rows value")
        if self.ledColumns > self.columns:
            raise TypeError("LedMatrix: columns value must be >= led columns value")
        """ create Matrix object as array of Strip objects """
        self.row = [ LedStrip( columns, pixelType=pixelType ) for x in range ( rows ) ]
        """ HW architecture - Left->Right, Right->Left for odd,even rows """
        self.matrixHW = matrixHW
    # some settings impacting all pixel        

    def setall2brightness(self, brightness=127):
        [ self.row[x].setall2brightness( brightness ) for x in range ( self.rows ) ]
        
    def setall2off(self):
        [ self.row[x].setall2off( ) for x in range ( self.rows ) ]

    def setall2color(self, red=0, green=0, blue=0, brightness=None):
        [ self.row[x].setall2color( red, green, blue, brightness ) for x in range ( self.rows ) ]

    def changeall2color(self, new_red=0, new_green=0, new_blue=0, new_brightness=None):
        [ self.row[x].changeall2color( new_red, new_green, new_blue, new_brightness ) for x in range ( self.rows ) ]

    def setall2random(self):
        [ self.row[x].setall2random( ) for x in range ( self.rows ) ]

    # some settings impacting one or many pixels        

    def set2color( self, row=1, column=1, red=84, green=84, blue=84, brightness=None):
        if type( row ) is not int or row < 1 or row  > self.rows:
            raise TypeError("pixel does'nt fit in matrix rows")
        if type( column ) is not int or column < 1 or column > self.columns:
            raise TypeError("pixel does'nt fit in matrix columns")
        self.row[row-1].pixel[column-1].set2color( red, green, blue, brightness )

    def setrow2color( self, row=1, red=84, green=84, blue=84, brightness=None):
        if type( row ) is not int or row < 1 or row  > self.rows:
            raise TypeError("row does'nt fit in matrix rows")
        self.row[row-1].setall2color( red, green, blue, brightness ) 

    def setcolumn2color( self, column=1, red=84, green=84, blue=84, brightness=None):
        if type( column ) is not int or column < 1 or column > self.columns:
            raise TypeError("column does'nt fit in matrix columns")
        [ self.set2color( row+1, column, red, green, blue, brightness )
          for row in range ( self.rows )]

    def setblock2color( self, startrow=1, startcolumn=1, 
                              endrow=None, endcolumn=None,
                              red=84, green=84, blue=84, brightness=None,
                              fill=True):
        if endrow is None: endrow = self.rows
        if endcolumn is None: endcolumn = self.columns
        if type( startrow ) is not int or startrow < 1 or startrow  > self.rows:
            raise TypeError("start row does'nt fit in matrix rows")
        if type( endrow ) is not int or endrow < 1 or endrow  > self.rows:
            raise TypeError("end row does'nt fit in matrix rows")
        if endrow < startrow:
            raise TypeError("end row is smaller than start row")
        if type( startcolumn ) is not int or startcolumn < 1 or startcolumn > self.columns:
            raise TypeError("start column does'nt fit in matrix columns")
        if type( endcolumn ) is not int or endcolumn < 1 or endcolumn > self.columns:
            raise TypeError("end column does'nt fit in matrix columns")
        if endcolumn < startcolumn:
            raise TypeError("end column is smaller than start column")
        if fill:
            [ self.set2color( row, column, red, green, blue, brightness )
              for row in range ( startrow,endrow+1 ) for column in range ( startcolumn,endcolumn+1 )]
        else:
            [ self.set2color( row, column, red, green, blue, brightness )
              for row in range ( startrow,endrow+1 ) for column in [ startcolumn,endcolumn ]]
            [ self.set2color( row, column, red, green, blue, brightness )
              for row in [ startrow,endrow ] for column in range ( startcolumn,endcolumn+1 )]


    # some movings of all pixels

    def rotate_left(self):
        [ self.row[x].pixel.append(self.row[x].pixel.pop(0))
          for x in range ( self.rows ) ]
             
    def rotate_right(self):
        [ self.row[x].pixel.insert(0,self.row[x].pixel.pop(self.columns - 1))
          for x in range ( self.rows ) ]
             
    def rotate_up(self):
        self.row.append(self.row.pop(0))
             
    def rotate_down(self):
        self.row.insert(0,self.row.pop(self.rows - 1))
             
    def flip_horizontal(self):
        [ self.row[x].pixel.reverse() for x in range ( self.rows ) ]
                 
    def flip_vertical(self):
        self.row.reverse()


    # some text display

    def set5x3char( self, char=' ', startrow=1, startcolumn=1, red=84, green=84, blue=84, brightness=None):
        if type( startrow ) is not int or startrow < 1 or startrow + 4 > self.rows:
            raise TypeError("character doesn't fit in matrix rows")
        if type( startcolumn ) is not int or startcolumn < 1 or startcolumn + 3 > self.columns:
            raise TypeError("character doesn't fit in matrix columns")
        for x in range(5):
            for y in range(3):
                if char_5x3[char][x][y] == 0:
                    self.row[startrow+x-1].pixel[startcolumn+y-1].set2off( )
                else:
                    self.row[startrow+x-1].pixel[startcolumn+y-1].set2color( red, green, blue, brightness )

    def set5x3text( self, text=' ', red=84, green=84, blue=84, brightness=None, replace=False):
        if not replace:
            self.setall2off()
        column = 1
        for char in text:
            if not ( replace and char == ' ' ):
                self.set5x3char( char, 1, column, red, green, blue, brightness )
            column += 4

    def set7x5char( self, char=' ', startrow=1, startcolumn=1, red=84, green=84, blue=84, brightness=None ):
        if type( startrow ) is not int or startrow < 1 or startrow + 6 > self.rows:
            raise TypeError("character does'nt fit in matrix rows")
        if type( startcolumn ) is not int or startcolumn < 1 or startcolumn + 4 > self.columns:
            raise TypeError("character does'nt fit in matrix columns")
        for x in range(7):
            for y in range(5):
                if char_7x5[char][x][y] == 0:
                    self.row[startrow+x-1].pixel[startcolumn+y-1].set2off( )
                else:
                    self.row[startrow+x-1].pixel[startcolumn+y-1].set2color( red, green, blue, brightness )

    def set7x5text( self, text=' ', red=84, green=84, blue=84, brightness=None, replace=False):
        if not replace:
            self.setall2off()
        column = 1
        for char in text:
            if not ( replace and char == ' ' ):
                self.set7x5char( char, 1, column, red, green, blue, brightness )
            column += 6

    def show( self, startRow=1, startColumn=1):
        # limit power consumption
        power_total = 0
        # keep window in virtual matrix
        startRow = ( startRow - 1 ) % ( self.rows - self.ledRows + 1 ) + 1
        startColumn = ( startColumn - 1 ) % ( self.columns - self.ledColumns + 1 ) + 1
        # pixel data   
        pixelData = []  
        for x in range ( self.ledRows ):
            row = ( startRow - 1 ) + x
            for y in range ( self.ledColumns ):
                direction = self.matrixHW[(x%2)*2:(x%2)*2+2]
                assert direction in ['LR','RL'], 'matrixHW parameter LR / RL only'
                if ( direction == 'LR' ):
                    column = ( startColumn - 1 ) + y
                else:
                    column = ( startColumn - 1 ) + ( self.ledColumns - 1 ) - y 
                #
                power_total += self.row[ row ].pixel[ column ].power_used
                if power_total <= self.power_limit:
                    pixelData += self.row[ row ].pixel[ column ].databytes
                else:
                    if self.pixelType in [ 'SK9822','APA102C']:
                        pixelData += [ 0xF0, 0x00, 0x00, 0x00 ]
                    if self.pixelType in [ 'NeoPixel']:
                        pixelData += [ 0x00, 0x00, 0x00 ]
        # sent to output
        ledStripUpdate(self.pixelType, pixelData )

class LedMatrix( LedVirtualMatrix ):
    """ define a matrix same size as an array of strips """
    def __init__( self, rows=2, columns=2,
                  pixelType='SK9822', matrixHW=matrixHWdefault, power_limit=max_power):
        """ create memory matrix same size as led array"""
        super().__init__( rows, columns, rows, columns,
                                   pixelType, matrixHW, power_limit )

    def show(self):
        super().show( 1, 1 )
           
if __name__ == "__main__":
    import sys
    import math
    if len(sys.argv) == 2:
        print ( "LedStripTest" )
        strip = LedStrip(int(sys.argv[1]))
        strip.setall2off()
        delay = 1 / math.sqrt(50 * strip.pixels)
        for runs in range(2):
            strip.pixel[0].set2green()
            strip.pixel[1].set2yellow()
            strip.pixel[2].set2blue()
            for x in range(3,strip.pixels):
                strip.rotate_right()
                strip.show()
                time.sleep(delay)
            strip.pixel[strip.pixels - 1].set2green()
            strip.pixel[strip.pixels - 2].set2yellow()
            strip.pixel[strip.pixels - 3].set2blue()
            for x in range(3,strip.pixels):
                strip.rotate_left()
                strip.show()
                time.sleep(delay)
        strip.setall2off()
        strip.show()
        print( "bye" )
    elif len(sys.argv) == 3:
        print ( "LedMatrixTest" )
        matrix = LedMatrix(int(sys.argv[1]),int(sys.argv[2]))
        for runs in range( 25 ):
            matrix.setall2random()
            matrix.show()
            time.sleep( 0.2 )
        matrix.setall2off()
        matrix.show()
        print( "bye" )
    else:
        print ( " usage: myLeds <pixel> or myLeds <rows> <colums>" )
           
        
                       

    

    
    

    

    



