# machind dependent SPI access

try:
    import rp2
    # SPI on Pico
    import machine
    spi_sck=machine.Pin(18)
    spi_tx=machine.Pin(19)
    spi_rx=machine.Pin(16)
    spi=machine.SPI(0,baudrate=1000000,sck=spi_sck, mosi=spi_tx, miso=spi_rx)
    #
    def SPIwrite(databytes):
        spi.write(bytes(databytes))
    #
except ImportError:
    # SPI on PI
    import spidev
    spi = spidev.SpiDev()
    #
    def SPIwrite(databytes):
        spi.open(0, 1)
        spi.max_speed_hz=8000000
        spi.xfer2( databytes )
        spi.close()
