from myLeds import LedStrip
from time import sleep

leds = 210
strip = LedStrip(leds)
strip.setall2off()
strip.pixel[0].set2green()
strip.show()
sleep(0.2)
for i in range(leds-1):
    strip.rotate_right()
    strip.show()
    sleep(0.2)
strip.setall2off()
strip.show()

    
